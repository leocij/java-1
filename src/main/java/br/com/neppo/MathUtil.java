package br.com.neppo;

public class MathUtil {

    /**
     * Dado um conjunto de números inteiros "ints" e um número arbitrário "sum",
     * retorne true caso exista pelo menos um subconjunto de "ints" cuja soma soma dos seus elementos
     * seja igual a "sum"
     *
     * @param ints Conjunto de inteiros
     * @param sum Soma para o subconjunto
     * @return true ou false
     * @throws IllegalArgumentException caso o argumento "ints" seja null
     */
    public static boolean subsetSumChecker(int ints[], int sum) throws Exception {
        if(ints == null) {
            throw new UnsupportedOperationException("Attention --> ints is null!");
        }else {
            int cont = 0;
            if(cont == sum) return true;

            for (int anInt : ints) {
                cont += anInt;
                if (cont == sum) {
                    return true;
                }
            }
            return false;
        }
    }

}
